#!/bin/bash
# Small VirtualBox image (stripped of all unnecessary packages) based on the latest version of Ubuntu Xenial (wholebits/ubuntu16.04-64) with Python2 (for Ansible) and Docker.
# use -f to force (re)creation of a box

if [ "$#" -gt 0 ] && [ "$@" == "-f" ]
  then
    vagrant destroy -f
    rm -rf .vagrant *.box
fi

vagrant box update
vagrant up

vagrant ssh -c "sudo sed -i '/tty/!s/mesg n/tty -s \\&\\& mesg n/' /root/.profile"
vagrant ssh -c "sudo touch /etc/apt/sources.list.d/docker.list"
vagrant ssh -c "sudo sed -i '/^GRUB_CMDLINE_LINUX=/s/=.*/=\"cgroup_enable=memory swapaccount=1\"/' /etc/default/grub && sudo update-grub"

# Remove Python3 in favour of Python2.7 and upgrade system
vagrant ssh -c "sudo apt-key update && sudo apt update && sudo apt autoremove -y && sudo DEBIAN_FRONTEND=noninteractive apt dist-upgrade -o \"Dpkg::Options::=--force-confdef\" -o Dpkg::Options::=\"--force-confold\" -y && sudo apt upgrade -o Dpkg::Options::=\"--force-confold\" -y"
vagrant ssh -c "sudo DEBIAN_FRONTEND=noninteractive apt install -o \"Dpkg::Options::=--force-confdef\" -o Dpkg::Options::=\"--force-confold\" -y -f --no-install-recommends make gcc byobu linux-image-extra-\$(uname -r) linux-image-extra-virtual unzip curl apt-transport-https ca-certificates"
vagrant ssh -c "sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D"
vagrant ssh -c "echo 'deb https://apt.dockerproject.org/repo ubuntu-xenial main' | sudo tee --append /etc/apt/sources.list.d/docker.list > /dev/null"
vagrant ssh -c "sudo apt update && sudo apt upgrade -y && sudo apt autoremove -y && sudo DEBIAN_FRONTEND=noninteractive apt install --no-install-recommends docker-engine -y"
vagrant ssh -c "sudo sed -i '/^ExecStart=/s/=.*/=\/usr\/bin\/dockerd -H fd:\/\/ --storage-driver=overlay2 --icc=false --iptables=true/' /lib/systemd/system/docker.service && sudo systemctl daemon-reload && sudo systemctl restart docker"

vagrant reload

# Clean up box
vagrant ssh -c "sudo purge-old-kernels --keep 1 -qy && sudo apt install -y python && sudo apt remove -y python3* byobu"
vagrant ssh -c "sudo find /var/log -type f -delete && sudo apt autoremove -y && sudo apt clean && sudo apt autoclean"
vagrant ssh -c "sudo dd if=/dev/zero of=/EMPTY bs=1M"
vagrant ssh -c "sudo rm -rf /EMPTY && cat /dev/null > ~/.bash_history && history -c && sudo history -c"

vagrant halt
vagrant package --output docker_ubuntu-xenial64.box

# Print information for post-processing
echo "Use 'curl -s -H \"X-Atlas-Token: \$ATLAS_TOKEN\" \"https://atlas.hashicorp.com/api/v1/box/boonen/docker_ubuntu-xenial64/version/{VERSION}/provider/virtualbox/upload\" to retrieve an upload token."
echo "Now use 'curl -X PUT --upload-file docker_ubuntu-xenial64.box https://binstore.hashicorp.com/{TOKEN}' to upload the file."

# vagrant ssh -c "dpkg -l | tail -n +6 | grep -E 'linux-image-[0-9]+' | grep -Fv \$(uname -r)"